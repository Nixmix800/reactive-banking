schema {
  query: query_root
  mutation: mutation_root
  subscription: subscription_root
}

"""whether this query should be cached (Hasura Cloud only)"""
directive @cached(
  """measured in seconds"""
  ttl: Int! = 60

  """refresh the cache entry"""
  refresh: Boolean! = false
) on QUERY

"""
Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'.
"""
input String_comparison_exp {
  _eq: String
  _gt: String
  _gte: String

  """does the column match the given case-insensitive pattern"""
  _ilike: String
  _in: [String!]

  """
  does the column match the given POSIX regular expression, case insensitive
  """
  _iregex: String
  _is_null: Boolean

  """does the column match the given pattern"""
  _like: String
  _lt: String
  _lte: String
  _neq: String

  """does the column NOT match the given case-insensitive pattern"""
  _nilike: String
  _nin: [String!]

  """
  does the column NOT match the given POSIX regular expression, case insensitive
  """
  _niregex: String

  """does the column NOT match the given pattern"""
  _nlike: String

  """
  does the column NOT match the given POSIX regular expression, case sensitive
  """
  _nregex: String

  """does the column NOT match the given SQL regular expression"""
  _nsimilar: String

  """
  does the column match the given POSIX regular expression, case sensitive
  """
  _regex: String

  """does the column match the given SQL regular expression"""
  _similar: String
}

"""mutation root"""
type mutation_root {
  """
  delete data from the table: "payments.Payments"
  """
  delete_payments_Payments(
    """filter the rows which have to be deleted"""
    where: payments_Payments_bool_exp!
  ): payments_Payments_mutation_response

  """
  delete single row from the table: "payments.Payments"
  """
  delete_payments_Payments_by_pk(id: uuid!): payments_Payments

  """
  delete data from the table: "transactions.Transactions"
  """
  delete_transactions_Transactions(
    """filter the rows which have to be deleted"""
    where: transactions_Transactions_bool_exp!
  ): transactions_Transactions_mutation_response

  """
  delete single row from the table: "transactions.Transactions"
  """
  delete_transactions_Transactions_by_pk(id: uuid!): transactions_Transactions

  """
  insert data into the table: "payments.Payments"
  """
  insert_payments_Payments(
    """the rows to be inserted"""
    objects: [payments_Payments_insert_input!]!

    """upsert condition"""
    on_conflict: payments_Payments_on_conflict
  ): payments_Payments_mutation_response

  """
  insert a single row into the table: "payments.Payments"
  """
  insert_payments_Payments_one(
    """the row to be inserted"""
    object: payments_Payments_insert_input!

    """upsert condition"""
    on_conflict: payments_Payments_on_conflict
  ): payments_Payments

  """
  insert data into the table: "transactions.Transactions"
  """
  insert_transactions_Transactions(
    """the rows to be inserted"""
    objects: [transactions_Transactions_insert_input!]!

    """upsert condition"""
    on_conflict: transactions_Transactions_on_conflict
  ): transactions_Transactions_mutation_response

  """
  insert a single row into the table: "transactions.Transactions"
  """
  insert_transactions_Transactions_one(
    """the row to be inserted"""
    object: transactions_Transactions_insert_input!

    """upsert condition"""
    on_conflict: transactions_Transactions_on_conflict
  ): transactions_Transactions

  """
  update data of the table: "payments.Payments"
  """
  update_payments_Payments(
    """increments the numeric columns with given value of the filtered values"""
    _inc: payments_Payments_inc_input

    """sets the columns of the filtered rows to the given values"""
    _set: payments_Payments_set_input

    """filter the rows which have to be updated"""
    where: payments_Payments_bool_exp!
  ): payments_Payments_mutation_response

  """
  update single row of the table: "payments.Payments"
  """
  update_payments_Payments_by_pk(
    """increments the numeric columns with given value of the filtered values"""
    _inc: payments_Payments_inc_input

    """sets the columns of the filtered rows to the given values"""
    _set: payments_Payments_set_input
    pk_columns: payments_Payments_pk_columns_input!
  ): payments_Payments

  """
  update data of the table: "transactions.Transactions"
  """
  update_transactions_Transactions(
    """increments the numeric columns with given value of the filtered values"""
    _inc: transactions_Transactions_inc_input

    """sets the columns of the filtered rows to the given values"""
    _set: transactions_Transactions_set_input

    """filter the rows which have to be updated"""
    where: transactions_Transactions_bool_exp!
  ): transactions_Transactions_mutation_response

  """
  update single row of the table: "transactions.Transactions"
  """
  update_transactions_Transactions_by_pk(
    """increments the numeric columns with given value of the filtered values"""
    _inc: transactions_Transactions_inc_input

    """sets the columns of the filtered rows to the given values"""
    _set: transactions_Transactions_set_input
    pk_columns: transactions_Transactions_pk_columns_input!
  ): transactions_Transactions
}

scalar numeric

"""
Boolean expression to compare columns of type "numeric". All fields are combined with logical 'AND'.
"""
input numeric_comparison_exp {
  _eq: numeric
  _gt: numeric
  _gte: numeric
  _in: [numeric!]
  _is_null: Boolean
  _lt: numeric
  _lte: numeric
  _neq: numeric
  _nin: [numeric!]
}

"""column ordering options"""
enum order_by {
  """in ascending order, nulls last"""
  asc

  """in ascending order, nulls first"""
  asc_nulls_first

  """in ascending order, nulls last"""
  asc_nulls_last

  """in descending order, nulls first"""
  desc

  """in descending order, nulls first"""
  desc_nulls_first

  """in descending order, nulls last"""
  desc_nulls_last
}

"""
columns and relationships of "payments.Payments"
"""
type payments_Payments {
  amount: numeric!
  creditorIban: String!
  debtorIban: String!
  description: String!
  id: uuid!
  state: String!
}

"""
aggregated selection of "payments.Payments"
"""
type payments_Payments_aggregate {
  aggregate: payments_Payments_aggregate_fields
  nodes: [payments_Payments!]!
}

"""
aggregate fields of "payments.Payments"
"""
type payments_Payments_aggregate_fields {
  avg: payments_Payments_avg_fields
  count(columns: [payments_Payments_select_column!], distinct: Boolean): Int!
  max: payments_Payments_max_fields
  min: payments_Payments_min_fields
  stddev: payments_Payments_stddev_fields
  stddev_pop: payments_Payments_stddev_pop_fields
  stddev_samp: payments_Payments_stddev_samp_fields
  sum: payments_Payments_sum_fields
  var_pop: payments_Payments_var_pop_fields
  var_samp: payments_Payments_var_samp_fields
  variance: payments_Payments_variance_fields
}

"""aggregate avg on columns"""
type payments_Payments_avg_fields {
  amount: Float
}

"""
Boolean expression to filter rows from the table "payments.Payments". All fields are combined with a logical 'AND'.
"""
input payments_Payments_bool_exp {
  _and: [payments_Payments_bool_exp!]
  _not: payments_Payments_bool_exp
  _or: [payments_Payments_bool_exp!]
  amount: numeric_comparison_exp
  creditorIban: String_comparison_exp
  debtorIban: String_comparison_exp
  description: String_comparison_exp
  id: uuid_comparison_exp
  state: String_comparison_exp
}

"""
unique or primary key constraints on table "payments.Payments"
"""
enum payments_Payments_constraint {
  """unique or primary key constraint"""
  Payments_pkey
}

"""
input type for incrementing numeric columns in table "payments.Payments"
"""
input payments_Payments_inc_input {
  amount: numeric
}

"""
input type for inserting data into table "payments.Payments"
"""
input payments_Payments_insert_input {
  amount: numeric
  creditorIban: String
  debtorIban: String
  description: String
  id: uuid
  state: String
}

"""aggregate max on columns"""
type payments_Payments_max_fields {
  amount: numeric
  creditorIban: String
  debtorIban: String
  description: String
  id: uuid
  state: String
}

"""aggregate min on columns"""
type payments_Payments_min_fields {
  amount: numeric
  creditorIban: String
  debtorIban: String
  description: String
  id: uuid
  state: String
}

"""
response of any mutation on the table "payments.Payments"
"""
type payments_Payments_mutation_response {
  """number of rows affected by the mutation"""
  affected_rows: Int!

  """data from the rows affected by the mutation"""
  returning: [payments_Payments!]!
}

"""
on_conflict condition type for table "payments.Payments"
"""
input payments_Payments_on_conflict {
  constraint: payments_Payments_constraint!
  update_columns: [payments_Payments_update_column!]! = []
  where: payments_Payments_bool_exp
}

"""Ordering options when selecting data from "payments.Payments"."""
input payments_Payments_order_by {
  amount: order_by
  creditorIban: order_by
  debtorIban: order_by
  description: order_by
  id: order_by
  state: order_by
}

"""primary key columns input for table: payments_Payments"""
input payments_Payments_pk_columns_input {
  id: uuid!
}

"""
select columns of table "payments.Payments"
"""
enum payments_Payments_select_column {
  """column name"""
  amount

  """column name"""
  creditorIban

  """column name"""
  debtorIban

  """column name"""
  description

  """column name"""
  id

  """column name"""
  state
}

"""
input type for updating data in table "payments.Payments"
"""
input payments_Payments_set_input {
  amount: numeric
  creditorIban: String
  debtorIban: String
  description: String
  id: uuid
  state: String
}

"""aggregate stddev on columns"""
type payments_Payments_stddev_fields {
  amount: Float
}

"""aggregate stddev_pop on columns"""
type payments_Payments_stddev_pop_fields {
  amount: Float
}

"""aggregate stddev_samp on columns"""
type payments_Payments_stddev_samp_fields {
  amount: Float
}

"""aggregate sum on columns"""
type payments_Payments_sum_fields {
  amount: numeric
}

"""
update columns of table "payments.Payments"
"""
enum payments_Payments_update_column {
  """column name"""
  amount

  """column name"""
  creditorIban

  """column name"""
  debtorIban

  """column name"""
  description

  """column name"""
  id

  """column name"""
  state
}

"""aggregate var_pop on columns"""
type payments_Payments_var_pop_fields {
  amount: Float
}

"""aggregate var_samp on columns"""
type payments_Payments_var_samp_fields {
  amount: Float
}

"""aggregate variance on columns"""
type payments_Payments_variance_fields {
  amount: Float
}

type query_root {
  """
  fetch data from the table: "payments.Payments"
  """
  payments_Payments(
    """distinct select on columns"""
    distinct_on: [payments_Payments_select_column!]

    """limit the number of rows returned"""
    limit: Int

    """skip the first n rows. Use only with order_by"""
    offset: Int

    """sort the rows by one or more columns"""
    order_by: [payments_Payments_order_by!]

    """filter the rows returned"""
    where: payments_Payments_bool_exp
  ): [payments_Payments!]!

  """
  fetch aggregated fields from the table: "payments.Payments"
  """
  payments_Payments_aggregate(
    """distinct select on columns"""
    distinct_on: [payments_Payments_select_column!]

    """limit the number of rows returned"""
    limit: Int

    """skip the first n rows. Use only with order_by"""
    offset: Int

    """sort the rows by one or more columns"""
    order_by: [payments_Payments_order_by!]

    """filter the rows returned"""
    where: payments_Payments_bool_exp
  ): payments_Payments_aggregate!

  """
  fetch data from the table: "payments.Payments" using primary key columns
  """
  payments_Payments_by_pk(id: uuid!): payments_Payments

  """
  fetch data from the table: "transactions.Transactions"
  """
  transactions_Transactions(
    """distinct select on columns"""
    distinct_on: [transactions_Transactions_select_column!]

    """limit the number of rows returned"""
    limit: Int

    """skip the first n rows. Use only with order_by"""
    offset: Int

    """sort the rows by one or more columns"""
    order_by: [transactions_Transactions_order_by!]

    """filter the rows returned"""
    where: transactions_Transactions_bool_exp
  ): [transactions_Transactions!]!

  """
  fetch aggregated fields from the table: "transactions.Transactions"
  """
  transactions_Transactions_aggregate(
    """distinct select on columns"""
    distinct_on: [transactions_Transactions_select_column!]

    """limit the number of rows returned"""
    limit: Int

    """skip the first n rows. Use only with order_by"""
    offset: Int

    """sort the rows by one or more columns"""
    order_by: [transactions_Transactions_order_by!]

    """filter the rows returned"""
    where: transactions_Transactions_bool_exp
  ): transactions_Transactions_aggregate!

  """
  fetch data from the table: "transactions.Transactions" using primary key columns
  """
  transactions_Transactions_by_pk(id: uuid!): transactions_Transactions
}

type subscription_root {
  """
  fetch data from the table: "payments.Payments"
  """
  payments_Payments(
    """distinct select on columns"""
    distinct_on: [payments_Payments_select_column!]

    """limit the number of rows returned"""
    limit: Int

    """skip the first n rows. Use only with order_by"""
    offset: Int

    """sort the rows by one or more columns"""
    order_by: [payments_Payments_order_by!]

    """filter the rows returned"""
    where: payments_Payments_bool_exp
  ): [payments_Payments!]!

  """
  fetch aggregated fields from the table: "payments.Payments"
  """
  payments_Payments_aggregate(
    """distinct select on columns"""
    distinct_on: [payments_Payments_select_column!]

    """limit the number of rows returned"""
    limit: Int

    """skip the first n rows. Use only with order_by"""
    offset: Int

    """sort the rows by one or more columns"""
    order_by: [payments_Payments_order_by!]

    """filter the rows returned"""
    where: payments_Payments_bool_exp
  ): payments_Payments_aggregate!

  """
  fetch data from the table: "payments.Payments" using primary key columns
  """
  payments_Payments_by_pk(id: uuid!): payments_Payments

  """
  fetch data from the table: "transactions.Transactions"
  """
  transactions_Transactions(
    """distinct select on columns"""
    distinct_on: [transactions_Transactions_select_column!]

    """limit the number of rows returned"""
    limit: Int

    """skip the first n rows. Use only with order_by"""
    offset: Int

    """sort the rows by one or more columns"""
    order_by: [transactions_Transactions_order_by!]

    """filter the rows returned"""
    where: transactions_Transactions_bool_exp
  ): [transactions_Transactions!]!

  """
  fetch aggregated fields from the table: "transactions.Transactions"
  """
  transactions_Transactions_aggregate(
    """distinct select on columns"""
    distinct_on: [transactions_Transactions_select_column!]

    """limit the number of rows returned"""
    limit: Int

    """skip the first n rows. Use only with order_by"""
    offset: Int

    """sort the rows by one or more columns"""
    order_by: [transactions_Transactions_order_by!]

    """filter the rows returned"""
    where: transactions_Transactions_bool_exp
  ): transactions_Transactions_aggregate!

  """
  fetch data from the table: "transactions.Transactions" using primary key columns
  """
  transactions_Transactions_by_pk(id: uuid!): transactions_Transactions
}

scalar timestamptz

"""
Boolean expression to compare columns of type "timestamptz". All fields are combined with logical 'AND'.
"""
input timestamptz_comparison_exp {
  _eq: timestamptz
  _gt: timestamptz
  _gte: timestamptz
  _in: [timestamptz!]
  _is_null: Boolean
  _lt: timestamptz
  _lte: timestamptz
  _neq: timestamptz
  _nin: [timestamptz!]
}

"""This table contains all transactions"""
type transactions_Transactions {
  amount: numeric!
  creditorIban: String!
  debtorIban: String!
  description: String!
  executionDate: timestamptz!
  id: uuid!
}

"""
aggregated selection of "transactions.Transactions"
"""
type transactions_Transactions_aggregate {
  aggregate: transactions_Transactions_aggregate_fields
  nodes: [transactions_Transactions!]!
}

"""
aggregate fields of "transactions.Transactions"
"""
type transactions_Transactions_aggregate_fields {
  avg: transactions_Transactions_avg_fields
  count(columns: [transactions_Transactions_select_column!], distinct: Boolean): Int!
  max: transactions_Transactions_max_fields
  min: transactions_Transactions_min_fields
  stddev: transactions_Transactions_stddev_fields
  stddev_pop: transactions_Transactions_stddev_pop_fields
  stddev_samp: transactions_Transactions_stddev_samp_fields
  sum: transactions_Transactions_sum_fields
  var_pop: transactions_Transactions_var_pop_fields
  var_samp: transactions_Transactions_var_samp_fields
  variance: transactions_Transactions_variance_fields
}

"""aggregate avg on columns"""
type transactions_Transactions_avg_fields {
  amount: Float
}

"""
Boolean expression to filter rows from the table "transactions.Transactions". All fields are combined with a logical 'AND'.
"""
input transactions_Transactions_bool_exp {
  _and: [transactions_Transactions_bool_exp!]
  _not: transactions_Transactions_bool_exp
  _or: [transactions_Transactions_bool_exp!]
  amount: numeric_comparison_exp
  creditorIban: String_comparison_exp
  debtorIban: String_comparison_exp
  description: String_comparison_exp
  executionDate: timestamptz_comparison_exp
  id: uuid_comparison_exp
}

"""
unique or primary key constraints on table "transactions.Transactions"
"""
enum transactions_Transactions_constraint {
  """unique or primary key constraint"""
  Transactions_pkey
}

"""
input type for incrementing numeric columns in table "transactions.Transactions"
"""
input transactions_Transactions_inc_input {
  amount: numeric
}

"""
input type for inserting data into table "transactions.Transactions"
"""
input transactions_Transactions_insert_input {
  amount: numeric
  creditorIban: String
  debtorIban: String
  description: String
  executionDate: timestamptz
  id: uuid
}

"""aggregate max on columns"""
type transactions_Transactions_max_fields {
  amount: numeric
  creditorIban: String
  debtorIban: String
  description: String
  executionDate: timestamptz
  id: uuid
}

"""aggregate min on columns"""
type transactions_Transactions_min_fields {
  amount: numeric
  creditorIban: String
  debtorIban: String
  description: String
  executionDate: timestamptz
  id: uuid
}

"""
response of any mutation on the table "transactions.Transactions"
"""
type transactions_Transactions_mutation_response {
  """number of rows affected by the mutation"""
  affected_rows: Int!

  """data from the rows affected by the mutation"""
  returning: [transactions_Transactions!]!
}

"""
on_conflict condition type for table "transactions.Transactions"
"""
input transactions_Transactions_on_conflict {
  constraint: transactions_Transactions_constraint!
  update_columns: [transactions_Transactions_update_column!]! = []
  where: transactions_Transactions_bool_exp
}

"""Ordering options when selecting data from "transactions.Transactions"."""
input transactions_Transactions_order_by {
  amount: order_by
  creditorIban: order_by
  debtorIban: order_by
  description: order_by
  executionDate: order_by
  id: order_by
}

"""primary key columns input for table: transactions_Transactions"""
input transactions_Transactions_pk_columns_input {
  id: uuid!
}

"""
select columns of table "transactions.Transactions"
"""
enum transactions_Transactions_select_column {
  """column name"""
  amount

  """column name"""
  creditorIban

  """column name"""
  debtorIban

  """column name"""
  description

  """column name"""
  executionDate

  """column name"""
  id
}

"""
input type for updating data in table "transactions.Transactions"
"""
input transactions_Transactions_set_input {
  amount: numeric
  creditorIban: String
  debtorIban: String
  description: String
  executionDate: timestamptz
  id: uuid
}

"""aggregate stddev on columns"""
type transactions_Transactions_stddev_fields {
  amount: Float
}

"""aggregate stddev_pop on columns"""
type transactions_Transactions_stddev_pop_fields {
  amount: Float
}

"""aggregate stddev_samp on columns"""
type transactions_Transactions_stddev_samp_fields {
  amount: Float
}

"""aggregate sum on columns"""
type transactions_Transactions_sum_fields {
  amount: numeric
}

"""
update columns of table "transactions.Transactions"
"""
enum transactions_Transactions_update_column {
  """column name"""
  amount

  """column name"""
  creditorIban

  """column name"""
  debtorIban

  """column name"""
  description

  """column name"""
  executionDate

  """column name"""
  id
}

"""aggregate var_pop on columns"""
type transactions_Transactions_var_pop_fields {
  amount: Float
}

"""aggregate var_samp on columns"""
type transactions_Transactions_var_samp_fields {
  amount: Float
}

"""aggregate variance on columns"""
type transactions_Transactions_variance_fields {
  amount: Float
}

scalar uuid

"""
Boolean expression to compare columns of type "uuid". All fields are combined with logical 'AND'.
"""
input uuid_comparison_exp {
  _eq: uuid
  _gt: uuid
  _gte: uuid
  _in: [uuid!]
  _is_null: Boolean
  _lt: uuid
  _lte: uuid
  _neq: uuid
  _nin: [uuid!]
}


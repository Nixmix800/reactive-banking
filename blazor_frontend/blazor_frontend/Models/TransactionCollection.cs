﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using blazor_frontend.Annotations;

namespace blazor_frontend.Models;

public class TransactionCollection : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler? PropertyChanged;

    private readonly ObservableCollection<Transaction> _Transactions;

    public TransactionCollection()
    {
        _Transactions = new ObservableCollection<Transaction>();
        _Transactions.CollectionChanged += HandleChange;
    }

    private void HandleChange(object sender, NotifyCollectionChangedEventArgs e)
    {
        foreach (Transaction item in e.NewItems!)
        {
            Console.WriteLine(item.CreditorIban);
        }
    }

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
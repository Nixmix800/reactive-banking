﻿namespace blazor_frontend.Models;

public class CreatePaymentResult
{
    public InsertPayments Insert_Payments_Payments { get; set; }
}

public class CreatePayment
{
    public string? Id { get; set; }
}
    
public class InsertPayments
{
    public IEnumerable<CreatePayment> Returning { get; set; }
}
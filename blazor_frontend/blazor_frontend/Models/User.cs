﻿using System.ComponentModel.DataAnnotations;

namespace blazor_frontend.Models;

public class User
{
    [Required]
    public string Iban { get; set; }

    public object ConvertToGqlRequest()
    {
        return new
        {
            UserID = Iban
        };
    }
}
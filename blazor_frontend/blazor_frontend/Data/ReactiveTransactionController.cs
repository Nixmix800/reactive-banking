﻿using System.Collections.ObjectModel;
using blazor_frontend.Models;
using GraphQL;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;

namespace blazor_frontend.Data;

public class ReactiveTransactionController : IDisposable
{
    private readonly ILogger<ReactiveTransactionController> _logger;
    private readonly GraphQLHttpClient _graphQlClient;
    
    private IDisposable _paymentSubscription;
    private IDisposable _transactionSubscription;

    private User _user;

    private ObservableCollection<Transaction> _observableTransactions;
    private ObservableCollection<Payment> _observablePayments;

    private const string PaymentsSubscriptionString = @"
        subscription Payments($id: uuid!) {
            payments_Payments(where: {id: {_eq: $id}}) {
                state
            }
        }";

    private const string TransactionSubscriptionString = @"
        subscription Transactions($limit: Int = 10, $userID: String!) {
          transactions_Transactions(limit: $limit, order_by: {executionDate: desc}, where: {_or: [{debtorIban: {_eq: $userID}}, {creditorIban: {_eq: $userID}}]}) {
            amount
            creditorIban
            debtorIban
            description
            executionDate
            id
          }
        }";

    private const string AllPaymentsSubscriptionString = @"
        subscription AllPayments($limit: Int = 10, $userID: String!) {
          payments_Payments(limit: $limit, where: {_or: [{debtorIban: {_eq: $userID}}, {creditorIban: {_eq: $userID}}]}) {
            amount
            creditorIban
            debtorIban
            description
            state
            id
          }
        }";

    private const string CreatePaymentString = @"
        mutation CreatePayment($amount: numeric!, $creditorIban: String!, $debtorIban: String!, $description: String!) {
          insert_payments_Payments(objects: {amount: $amount, creditorIban: $creditorIban, debtorIban: $debtorIban, description: $description}) {
            returning {
              id
            }
          }
        }";

    public ReactiveTransactionController(IConfiguration configuration, ILogger<ReactiveTransactionController> logger)
    {
        _logger = logger;
        _graphQlClient = new GraphQLHttpClient(configuration["HasuraUrl"], new NewtonsoftJsonSerializer());

        _observableTransactions = new ObservableCollection<Transaction>();
        _observablePayments = new ObservableCollection<Payment>();
    }
    
    public bool SetUser(User user)
    {
        if (string.IsNullOrEmpty(user.Iban))
        {
            return false;
        }
        _user = user;
        
        var transactionStream = _graphQlClient.CreateSubscriptionStream<TransactionResults>
            (new GraphQLRequest(TransactionSubscriptionString, _user.ConvertToGqlRequest()));
        _transactionSubscription = transactionStream.Subscribe(async (response) =>
            await ProcessTransactions(response.Data.transactions_Transactions));
        
        var paymentsStream = _graphQlClient.CreateSubscriptionStream<PaymentResults>
            (new GraphQLRequest(AllPaymentsSubscriptionString, _user.ConvertToGqlRequest()));
        _paymentSubscription = paymentsStream.Subscribe(async (response) => 
            await ProcessPayments(response.Data.payments_Payments));
        
        return true;
    }
    
    public void SetPayment(Payment payment)
    {
        var paymentsStream = _graphQlClient.CreateSubscriptionStream<PaymentResults>
            (new GraphQLRequest(PaymentsSubscriptionString, payment.ConvertToGqlRequest()));
        _paymentSubscription = paymentsStream.Subscribe(async (response) => 
            await ProcessPayments(response.Data.payments_Payments));
    }

    private async Task ProcessPayments(IEnumerable<Payment> payments)
    {
        //Update collection of transactions here
        _observablePayments.Clear();
        foreach (var payment in payments)
        {
            _observablePayments.Add(payment);
        }
    }
    
    private async Task ProcessTransactions(IEnumerable<Transaction> transactions)
    {
        //Update collection of transactions here
        _observableTransactions.Clear();
        foreach (var transaction in transactions)
        {
            _observableTransactions.Add(transaction);
        }
    }

    public ObservableCollection<Transaction> GetObservableTransaction()
    {
        return _observableTransactions;
    }
    
    public ObservableCollection<Payment> GetObservablePayment()
    {
        return _observablePayments;
    }
    
    public async Task<string?> CreatePaymentAsync(Payment payment)
    {
        var request = new GraphQLRequest(CreatePaymentString, payment.ConvertToGqlRequest());
        var response = await _graphQlClient
            .SendMutationAsync<CreatePaymentResult>(request);
        
        _logger.LogInformation("Created transaction with id: {0}.", response.Data.Insert_Payments_Payments.Returning.First().Id);

        return response.Data.Insert_Payments_Payments.Returning.First().Id;
    }
    
    public void Dispose()
    {
        _paymentSubscription.Dispose();
        _transactionSubscription.Dispose();
    }
}

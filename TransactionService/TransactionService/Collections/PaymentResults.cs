﻿using TransactionService.Models;

namespace TransactionService.Collections;

public class PaymentResults
{
    public IEnumerable<Payment> payments_Payments { get; set; }
}
using Microsoft.AspNetCore.Mvc;
using TransactionService.Models;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace TransactionService.Controllers;

[ApiController]
[Route("[controller]")]
public class TransactionController : ControllerBase
{

    private readonly ILogger<TransactionController> _logger;
    private readonly ReactiveTransactionController _transactionManager;

    public TransactionController(ILogger<TransactionController> logger, ReactiveTransactionController transactionManager)
    {
        _logger = logger;
        _transactionManager = transactionManager;
    }

    [HttpPost]
    [Route("api/[controller]/")]
    [SwaggerOperation("SubmitTransaction")]
    [SwaggerResponse(statusCode: 200, type: typeof(Payment),
        description: "Successfully submitted a new transaction")]
    [SwaggerResponse(statusCode: 500, type: typeof(Error), description: "An error occured.")]
    public async Task<IActionResult> SubmitTransaction([FromBody] Transaction transaction)
    {
        try
        {
            transaction.ExecutionDate = DateTime.Now;
            var result = await _transactionManager.CreateTransactionAsync(transaction);
            
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(StatusCode(500, new Error
                {
                    ErrorMessage = $"An error occured during creation of transaction."
                }));
            }
        }
        catch (Exception e)
        {
            return BadRequest(StatusCode(500, new Error
            {
                ErrorMessage = $"An error occured.\n{e.Message}"
            }));
        }
    }
}
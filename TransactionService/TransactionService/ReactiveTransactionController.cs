﻿using GraphQL;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using TransactionService.Collections;
using TransactionService.Models;
using TransactionService.Mutations;

namespace TransactionService;

public class ReactiveTransactionController : IDisposable
{
    private readonly ILogger<ReactiveTransactionController> _logger;
    private readonly GraphQLHttpClient _graphQlClient;
    
    private readonly IDisposable _subscription;

    private const string PaymentsSubscriptionString = @"
        subscription Payments {
          payments_Payments(where: {state: {_eq: ""open""}}) {
            amount
            creditorIban
            debtorIban
            description
            id
            state
            }
        }";

    private const string CreateTransactionString = @"
        mutation CreateTransaction($amount: numeric!, $creditorIban: String!, $debtorIban: String!, $description: String, $executionDate: timestamptz!) {
          insert_transactions_Transactions(objects: {amount: $amount, creditorIban: $creditorIban, debtorIban: $debtorIban, description: $description, executionDate: $executionDate}) {
            returning {
              id
            }
          }
        }";
    
    private const string UpdatePaymentStateString = @"
        mutation UpdatePaymentState($_eq: uuid!, $state: String!) {
            update_payments_Payments(_set: {state: $state}, where: {id: {_eq: $_eq}}) {
                returning {
                  id
                }
            }
        }";

    public ReactiveTransactionController(IConfiguration configuration, ILogger<ReactiveTransactionController> logger)
    {
        _logger = logger;
        _graphQlClient = new GraphQLHttpClient(configuration["HasuraUrl"], new NewtonsoftJsonSerializer());
        
        var paymentsStream = _graphQlClient.CreateSubscriptionStream<PaymentResults>
            (new GraphQLRequest(PaymentsSubscriptionString));
        _subscription = paymentsStream.Subscribe(async (response) => 
            await ProcessPayments(response.Data.payments_Payments));
    }

    private async Task ProcessPayments(IEnumerable<Payment> payments)
    {
        foreach (var payment in payments)
        {
            if (payment.Id == null)
                break;
            var transaction = Transaction.FromPayment(payment);
            
            Thread.Sleep(3000);
            
            await CreateTransactionAsync(transaction);
            await UpdatePaymentStateAsync(payment.Id, "processed");
        }
    }

    public async Task<string?> CreateTransactionAsync(Transaction transaction)
    {
        var request = new GraphQLRequest(CreateTransactionString, transaction.ConvertToGqlRequest());
        var response = await _graphQlClient
            .SendMutationAsync<CreateTransactionResult>(request);
        
        _logger.LogInformation("Created transaction with id: {0}.", response.Data.Insert_Transactions_Transactions.Returning.First().Id);

        return response.Data.Insert_Transactions_Transactions.Returning.First().Id;
    }

    private async Task UpdatePaymentStateAsync(string paymentId, string newState)
    {
        var request = new GraphQLRequest(UpdatePaymentStateString, new
        {
            _eq = paymentId,
            state = newState
        });
        var response = await _graphQlClient
            .SendMutationAsync<UpdatePaymentState>(request);
        
        _logger.LogInformation("Updated payment ({0}) with state {1}.", response.Data.Update_Payments_Payments.Returning.First().Id, newState);
    }

    public void Dispose()
    {
        _subscription.Dispose();
    }
}

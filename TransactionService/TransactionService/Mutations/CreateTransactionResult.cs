﻿namespace TransactionService.Mutations;

public class CreateTransactionResult
{
    public InsertTransactions Insert_Transactions_Transactions { get; set; }
}

public class CreateTransaction
{
    public string? Id { get; set; }
}
    
public class InsertTransactions
{
    public IEnumerable<CreateTransaction> Returning { get; set; }
}
﻿namespace TransactionService.Mutations;

public class UpdatePaymentState
{
    public UpdatePayments Update_Payments_Payments { get; set; }
}

public class UpdatePayment
{
    public string? Id { get; set; }
}
    
public class UpdatePayments
{
    public IEnumerable<UpdatePayment> Returning { get; set; }
}

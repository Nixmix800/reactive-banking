﻿namespace TransactionService.Models;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

[DataContract]
public class Transaction
{
    [Required]
    [DataMember(Name = "debtor")]
    public string DebtorIban { get; set; } = null!;
    
    [Required]
    [DataMember(Name = "amount")]
    public double Amount { get; set; }
    
    [DataMember(Name = "description")]
    public string? Description { get; set; }
    
    [Required]
    [DataMember(Name = "creditor")]
    public string CreditorIban { get; set; } = null!;
    
    [DataMember(Name = "executionDate")]
    public DateTime ExecutionDate { get; set; }

    public static Transaction FromPayment(Payment payment)
    {
        return new Transaction
        {
            DebtorIban = payment.DebtorIban,
            Amount = payment.Amount,
            Description = payment.Description,
            CreditorIban = payment.CreditorIban,
            ExecutionDate = DateTime.Now
        };
    }

    public object ConvertToGqlRequest()
    {
        return new
        {
            CreditorIban,
            DebtorIban,
            Amount,
            Description,
            ExecutionDate
        };
    }
}
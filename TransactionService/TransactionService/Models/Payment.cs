﻿namespace TransactionService.Models;

public class Payment
{
    public string? Id { get; set; }
    public string DebtorIban { get; set; }
    public double Amount { get; set; }
    public string? Description { get; set; }
    public string CreditorIban { get; set; }

    public Payment FromTransaction(Transaction transaction)
    {
        return new Payment
        {
            Amount = transaction.Amount,
            CreditorIban = transaction.CreditorIban,
            DebtorIban = transaction.DebtorIban,
            Description = transaction?.Description
        };
    }
}